# News

## About

The app provides a way for users to search for the latest news form various sources. The user can read a part of the article inside the app and also navigate to the article's website to read more.

## Technical Details

- The app is written in Swift
- Technologies: SwiftUI + Combine
- Architecture: MVVM
- Supports iOS 14.4 or higher
- Xcode 12.4 or higher

## Installation steps

- Clone the project
- Checkout develop branch

## Credits

- Loading images is done with the aid of AsyncImage library (https://github.com/V8tr/AsyncImage) which provides asynchronous loading and caching.
