//
//  Mocks.swift
//  NewsTests
//
//  Created by Mostafa Sandeed on 31/10/2021.
//

import Foundation
import Combine
@testable import News

class MockNetworkAgent: NetworkAgentProtocol {
    var callTracker = 0
    var batches: [[Article]]
    
    init() {
        batches = testArticles.chunked(into: 2)
    }
    
    func fetchArticles(from url: URL) -> AnyPublisher<NewsListResponse, Never> {
        if callTracker > batches.count - 1 { callTracker = 0 }
        let response = NewsListResponse(status: "ok", totalResults: 10, articles: batches[callTracker])
        callTracker += 1
        return CurrentValueSubject<NewsListResponse, Never>(response).eraseToAnyPublisher()
    }
}

let testArticles: [Article] = [
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID()),
    Article(source: Source(id: "", name: ""), author: "fake", title: "fake", description: "fake", url: "fake", urlToImage: "fake", publishedAt: "fake", content: "fake", id: UUID())
]

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
