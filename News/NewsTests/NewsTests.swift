//
//  NewsTests.swift
//  NewsTests
//
//  Created by Mostafa Sandeed on 29/10/2021.
//

import XCTest
@testable import News

class NewsTests: XCTestCase {
    var viewModel: NewsListViewModel!
    
    override func setUp() {
        viewModel = NewsListViewModel(networkAgent: MockNetworkAgent())
    }
    
    override func tearDown() {
        viewModel = nil
    }
    
    func testFetchArticles() {
        // First call
        viewModel.fetchArticles(for: "any")
        XCTAssertEqual(viewModel.resultsState.searchText, "any")
        XCTAssertEqual(viewModel.resultsState.totalResults, 10)
        XCTAssertEqual(viewModel.articles.count, 2)
        XCTAssertEqual(viewModel.resultsState.page, 2)
        // Second call
        viewModel.loadMore()
        XCTAssertEqual(viewModel.articles.count, 4)
        XCTAssertEqual(viewModel.resultsState.page, 3)
        // Third call
        viewModel.loadMore()
        XCTAssertEqual(viewModel.articles.count, 6)
        XCTAssertEqual(viewModel.resultsState.page, 4)
        // Fourth call
        viewModel.loadMore()
        XCTAssertEqual(viewModel.articles.count, 8)
        XCTAssertEqual(viewModel.resultsState.page, 5)
        // Fifth call
        viewModel.loadMore()
        XCTAssertEqual(viewModel.articles.count, 10)
        XCTAssertEqual(viewModel.resultsState.page, 6)
        // Sixth call - Should not call fetch
        viewModel.loadMore()
        XCTAssertEqual(viewModel.articles.count, 10)
        XCTAssertEqual(viewModel.resultsState.page, 6)
        
        // New Search
        viewModel.fetchArticles(for: "new")
        XCTAssertEqual(viewModel.resultsState.searchText, "new")
        XCTAssertEqual(viewModel.resultsState.totalResults, 10)
        XCTAssertEqual(viewModel.articles.count, 2)
        XCTAssertEqual(viewModel.resultsState.page, 2)
    }

}
