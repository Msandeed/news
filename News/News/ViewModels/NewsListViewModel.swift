//
//  NewsListViewModel.swift
//  News
//
//  Created by Mostafa Sandeed on 29/10/2021.
//

import Foundation
import Combine

struct ResultsState {
    var searchText: String
    var page: Int
    var totalResults: Int?
}

class NewsListViewModel: ObservableObject {
    private var networkAgent: NetworkAgentProtocol
    private var subscriptions = Set<AnyCancellable>()
    @Published var articles = [Article]()
    var resultsState = ResultsState(searchText: "", page: 0, totalResults: 0)
    
    init(networkAgent: NetworkAgentProtocol) {
        self.networkAgent = networkAgent
    }
    
    // MARK: - Fetching
    func fetchArticles(for keyword: String) {
        if keyword != resultsState.searchText { // New search, reset resultsState.
            articles = []
            resultsState.searchText = keyword
            resultsState.page = 1
            resultsState.totalResults = nil
        }
        
        let urlString = baseURL + "?q=\(keyword)&pageSize=20&page=\(resultsState.page)&apiKey=\(apiKey)"
        let url: URL! = URL(string:urlString)
        
        networkAgent.fetchArticles(from: url)
            .sink { (_) in
                print("Completed")
            } receiveValue: { [weak self] (response) in
                self?.onReceive(response: response)
            }
            .store(in: &subscriptions)
    }
    
    func loadMore() {
        if let totalResults = resultsState.totalResults, totalResults == articles.count {    // End of available results, no need to make API call
            return
        } else {
            fetchArticles(for: resultsState.searchText)
        }
    }
    
    // MARK: - Updating results
    private func onReceive(response: NewsListResponse) {
        resultsState.page += 1
        resultsState.totalResults = response.totalResults
        articles.append(contentsOf: response.articles)
    }
}
