//
//  ArticleView.swift
//  News
//
//  Created by Mostafa Sandeed on 30/10/2021.
//

import SwiftUI

struct ArticleView: View {
    var article: Article
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 8) {
                Text(article.title ?? "")
                    .font(.headline)
                    .bold()
                    .lineLimit(nil)
                
                AsyncImage(url: URL(string: article.urlToImage)!, placeholder: {Color.gray}) { (image) -> Image in
                    Image(uiImage: image).resizable()
                }
                .aspectRatio(contentMode: .fit)
                .frame(height: 200)
                
                Text(article.description ?? "")
                    .font(.subheadline)
                    .bold()
                
                Text("Author: \(article.author ?? "")")
                    .font(.subheadline)
                
                Text("Published At: \(article.publishedAt ?? "")")
                    .font(.subheadline)
                
                Link("Go to website", destination: URL(string: article.url)!)
            }
            .padding()
        }
    }
}
