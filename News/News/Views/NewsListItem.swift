//
//  NewsListItem.swift
//  News
//
//  Created by Mostafa Sandeed on 30/10/2021.
//

import SwiftUI

struct NewsListItem: View {
    var article: Article
    
    var body: some View {
        VStack(alignment: .leading) {
            AsyncImage(url: URL(string: article.urlToImage)!, placeholder: {Color.gray}) { (image) -> Image in
                Image(uiImage: image).resizable()
            }
            .aspectRatio(contentMode: .fit)
            .frame(height: 200)
            
            Text(article.title ?? "")
                .font(.headline)
                .bold()
            
            Text(article.source.name ?? "")
                .font(.subheadline)
        }
    }
}
