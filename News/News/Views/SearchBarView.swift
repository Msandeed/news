//
//  SearchBarView.swift
//  News
//
//  Created by Mostafa Sandeed on 30/10/2021.
//

import SwiftUI

struct SearchBarView: UIViewRepresentable {

    // MARK: - Variables
    @Binding var text: String
    var searchCaller: () -> Void

    // MARK: - UIVIew
    func makeUIView(context: UIViewRepresentableContext<SearchBarView>) -> UISearchBar {
        let searchBar = UISearchBar()
        searchBar.delegate = context.coordinator
        searchBar.placeholder = "Search News"
        searchBar.setShowsCancelButton(true, animated: true)
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBarView>) {
    }

    // MARK: - Configuring Coordinator
    func makeCoordinator() -> SearchBarView.Coordinator {
        Coordinator(parent: self)
    }

    class Coordinator: NSObject, UISearchBarDelegate {
        var parent: SearchBarView

        init(parent: SearchBarView) {
            self.parent = parent
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            parent.text = searchText
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            parent.searchCaller()
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchBar.text = ""
            parent.text = ""
            searchBar.resignFirstResponder()
        }
    }
}
