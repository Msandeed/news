//
//  ContentView.swift
//  News
//
//  Created by Mostafa Sandeed on 29/10/2021.
//

import SwiftUI

struct NewsListView: View {
    
    @ObservedObject var viewModel: NewsListViewModel
    @State var input = ""
    
    var body: some View {
        
        NavigationView {
            VStack {
                SearchBarView(text: $input, searchCaller: { viewModel.fetchArticles(for: input) })
                List(viewModel.articles) { article in
                    NavigationLink(destination: ArticleView(article: article)) {
                        NewsListItem(article: article)
                            .onAppear {
                                if viewModel.articles.last?.id == article.id {
                                    viewModel.loadMore()
                                }
                            }
                    }
                }
                .navigationTitle("News")
                .navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}

struct NewsListView_Previews: PreviewProvider {
    static var previews: some View {
        NewsListView(viewModel: NewsListViewModel(networkAgent: NetworkAgent()))
    }
}
