//
//  Models.swift
//  News
//
//  Created by Mostafa Sandeed on 29/10/2021.
//

import Foundation

struct NewsListResponse: Codable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

struct Article: Codable, Identifiable {
    let source: Source
    let author, title, description: String?
    let url: String
    let urlToImage: String
    let publishedAt: String?
    let content: String?
    var id: UUID
    
    enum CodingKeys: String, CodingKey {
        case source, author, title, description, url, urlToImage, publishedAt, content
      }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.source = try container.decode(Source.self, forKey: .source)
        self.author = try container.decode(String.self, forKey: .author)
        self.title = try container.decode(String.self, forKey: .title)
        self.description = try container.decode(String.self, forKey: .description)
        self.url = try container.decode(String.self, forKey: .url)
        self.urlToImage = try container.decode(String.self, forKey: .urlToImage)
        self.publishedAt = try container.decode(String.self, forKey: .publishedAt)
        self.content = try container.decode(String.self, forKey: .content)

        self.id = UUID()
    }
    
    internal init(source: Source, author: String?, title: String?, description: String?, url: String, urlToImage: String, publishedAt: String?, content: String?, id: UUID) {
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
        self.id = id
    }
}

struct Source: Codable {
    let id: String?
    let name: String?
}
