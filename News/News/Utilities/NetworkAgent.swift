//
//  NetworkAgent.swift
//  News
//
//  Created by Mostafa Sandeed on 30/10/2021.
//

import Foundation
import Combine

protocol NetworkAgentProtocol {
    func fetchArticles(from url: URL) -> AnyPublisher<NewsListResponse, Never>
}

class NetworkAgent: NetworkAgentProtocol {
    func fetchArticles(from url: URL) -> AnyPublisher<NewsListResponse, Never> {
        URLSession.shared.dataTaskPublisher(for: url)
            .map { return $0.data }
            .decode(type: NewsListResponse.self, decoder: JSONDecoder())
            .replaceError(with: NewsListResponse(status: "", totalResults: 0, articles: []))
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
